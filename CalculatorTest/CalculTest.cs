﻿using CalculatorForm;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace CalculatorTest
{
    [TestClass]
    public class CalculTest
    {
        [TestMethod]
        public void TestDivisionZero()
        {
            Calcul calcul = new Calcul();
            calcul.Operator = "/";
            calcul.Operation = "45/0";
            string excepted = "Impossible de diviser par 0";
            Assert.AreEqual(excepted, calcul.getResult());
        }

        [TestMethod]
        public void TestDivisionEntier()
        {
            Calcul calcul = new Calcul();
            calcul.Operator = "/";
            calcul.Operation = "12/4";
            double excepted = 12 / 4;
            Assert.AreEqual(excepted.ToString(), calcul.getResult());
        }

        [TestMethod]
        public void TestMultiplicationEntier()
        {
            Calcul calcul = new Calcul();
            calcul.Operator = "*";
            calcul.Operation = "30*5";
            double excepted = 30 * 5;
            Assert.AreEqual(excepted.ToString(), calcul.getResult());
        }

        [TestMethod]
        public void TestAdditionEntier()
        {
            Calcul calcul = new Calcul();
            calcul.Operator = "+";
            calcul.Operation = "12+7";
            double excepted = 12 + 7;
            Assert.AreEqual(excepted.ToString(), calcul.getResult());
        }

        [TestMethod]
        public void TestSoustractionEntier()
        {
            Calcul calcul = new Calcul();
            calcul.Operator = "-";
            calcul.Operation = "281-43";
            double excepted = 281 - 43;
            Assert.AreEqual(excepted.ToString(), calcul.getResult());
        }

        [TestMethod]
        public void TestDivisionVirgule()
        {
            Calcul calcul = new Calcul();
            calcul.Operator = "/";
            calcul.Operation = "24,8/8,9";
            double excepted = 24.8 / 8.9;
            Assert.AreEqual(excepted.ToString(), calcul.getResult());
        }

        [TestMethod]
        public void TestMultiplicationVirgule()
        {
            Calcul calcul = new Calcul();
            calcul.Operator = "*";
            calcul.Operation = "14,26*98,37";
            double excepted = 14.26 * 98.37;
            Assert.AreEqual(excepted.ToString(), calcul.getResult());
        }

        [TestMethod]
        public void TestAdditionVirgule()
        {
            Calcul calcul = new Calcul();
            calcul.Operator = "+";
            calcul.Operation = "13,5+54,6";
            double excepted = 13.5 + 54.6;
            Assert.AreEqual(excepted.ToString(), calcul.getResult());
        }

        [TestMethod]
        public void TestSoustractionVirgule()
        {
            Calcul calcul = new Calcul();
            calcul.Operator = "-";
            calcul.Operation = "281,51-27,124";
            double excepted = 281.51 - 27.124;
            Assert.AreEqual(excepted.ToString(), calcul.getResult());
        }

    }
}
