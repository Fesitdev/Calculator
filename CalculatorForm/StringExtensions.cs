﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculatorForm
{
    public static class StringExtension
    {
        public static string RemoveLast(this string str)
        {
            if (string.IsNullOrEmpty(str))
                return string.Empty;

            if (str.Length == 1)
                return "0";

            return str.Substring(0, str.Length - 1);   
        }
    }
}
