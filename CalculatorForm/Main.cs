﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CalculatorForm
{
    public partial class Main : Form
    {
        Calcul calcul = new Calcul();
        bool error = false;

        public Main()
        {
            InitializeComponent();
            resultText.Text = "0";
        }

        private void numberInput_Click(object sender, EventArgs e)
        {
            if (!(sender is Button))
                return;

            // Si l'utilisateur n'a pas encore saisi de chiffre ou qu'il y'a une erreur, on clear la textbox 
            // pour ensuite afficher le nombre saisi
            if (resultText.Text == "0" || error) 
            {
                resultText.Text = string.Empty;
                error = false;
            }

            resultText.Text += (sender as Button).Text;
        }

        private void operatorInput_Click(object sender, EventArgs e)
        {
            if (!(sender is Button))
                return;

            if(error)
            {
                error = false;
                resultText.Text = "0";
            }

            var button = sender as Button;

            if (!calcul.Operands.Contains(button.Text.FirstOrDefault())) // Verification que l'operateur est bien pris en compte par notre classe calcul
                return;

            // Si le dernier input de l'utilisateur est déjà un operateur ou une virgule, on l'enlève
            // pour laisser place au nouveau char
            if (calcul.Operands.Contains(resultText.Text.Last()) || resultText.Text.Last() == ',')
                resultText.Text = resultText.Text.RemoveLast();

            resultText.Text += (sender as Button).Text;
            calcul.Operator = (sender as Button).Text;
        }

        private void removeLast_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(calcul.Operation))
                return;

            resultText.Text = resultText.Text.RemoveLast();
        }

        // Event declencher lorsque le texte de la textBox change, afin d'eviter de devoir
        // set les deux à chaque fois
        private void resultText_TextChanged(object sender, EventArgs e)
        {
            calcul.Operation = resultText.Text;
        }

        private void clear_Click(object sender, EventArgs e)
        {
            resultText.Text = "0";
            calcul.Operator = string.Empty;
        }

        private void clearErr_Click(object sender, EventArgs e)
        {
            if(string.IsNullOrEmpty(calcul.Operator)) 
                resultText.Text = "0";
            else
            {
                // Je split mon string afin d'obtenir un tableau contenant les deux nombre du calcul
                string[] numbers = resultText.Text.Split(calcul.Operator[0]);
                string numberB = numbers[1];

                // Je change la valeur de mon string en retirant le deuxième nombre
                resultText.Text = resultText.Text.Substring(0, resultText.Text.Length - numberB.Length);
            }
        }

        private void calcul_Click(object sender, EventArgs e)
        {
            string result = calcul.getResult();

            if (!double.TryParse(result, out double number))
            {
                error = true;
                calcul.Operator = string.Empty;
            }

            resultText.Text = result;
        }
    }
}
