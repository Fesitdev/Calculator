﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculatorForm
{
    public class Calcul
    {
        string _operation;
        string _operator;
        static char[] _operands = { '+', '-', '*', '/' };

        public string Operation
        {
            get => _operation;
            set => _operation = value;
        }

        public string Operator
        {
            get => _operator;   
            set => _operator = value;   
        }

        public char[] Operands => _operands;

        public string getResult()
        {
            // Je split mon string d'operation avec l'operateur afin d'obtenir les deux nombre 
            string[] numbers = Operation.Split(Operator.First());
            double result = 0;
            double numberA;
            double numberB;

            // je vérifie que les deux chaines de caractère soit bien au format de nombre
            if (!double.TryParse(numbers[0], out numberA) || !double.TryParse(numbers[1], out numberB))
                return "Math error";

            // Switch sur l'operateur et calcul en fonction de celui-ci
            switch(Operator)
            {
                case "+":
                    result = numberA + numberB;
                    break;
                case "-":
                    result = numberA - numberB;
                    break;
                case "*":
                    result = numberA * numberB;
                    break;
                case "/":
                    if (numberB == 0)
                        return "Impossible de diviser par 0";
                    else 
                        result = numberA / numberB;
                    break;
            }

            Operator = "";

            return result.ToString(); 
        }
    }
}
